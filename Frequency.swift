func frequency(_ arr: [Character]) {
	var freq = [Character:Int]()
	for char in arr {
		freq[char] = (freq[char] ?? 0) + 1
	}
	for char in arr {
		if freq.isEmpty {
			break
		}
		guard let count = freq[char] else { continue }
		print("\(char) => \(count)")
		freq.removeValue(forKey: char)
	}
}

func frequency(_ string: String) {
	frequency(Array(string))
}

frequency("WARMACHINEROX")
